from __future__ import print_function

import os
from pyspark import SparkContext
from pyspark.sql import SparkSession
from pyspark.sql.functions import *
import sys
import re


def column_add(a, b):
    return a.__add__(b)


if __name__ == "__main__":
    if len(sys.argv) != 3:
        print("Usage: count  ", file=sys.stderr)
        exit(-1)
    spark = SparkSession.builder \
        .appName("Count") \
        .getOrCreate()
    # input_path = "s3://bamboo-stalk/bamboo-plastid/input-spark/000.txt"
    # output_path = "s3://bamboo-stalk/bamboo-plastid/output-spark/step0/"
    input_path = sys.argv[1]
    output_path = sys.argv[2]
    df_0 = spark.read.text(input_path).cache().withColumn('company', split(col("value"), '\|')[1]).withColumn('cas_product', split(col("value"), '\|')[2]).withColumn('price', split(col("value"), '\|')[3].cast("double")).withColumn('order', lit(1)).cache()

    df_order = df_0.groupBy("cas_product").pivot("company").sum("order").na.fill(0).cache()
    df_order = df_order.withColumn('total', reduce(column_add, (df_order[c] for c in df_order.columns[1:]))).cache()
    df_order_percent = df_order.select(*((df_order[col] / df_order['total']).alias(col) for col in df_order.columns[1:])).withColumn("cas_product",lit("df_order_percent"))
    df_order_percent_sum = df_order_percent.agg({col: "sum" for col in df_order.columns if col not in ['cas_product']}).cache()
    df_order_percent_sum = df_order_percent_sum.select(*(col(x).alias(re.search(".*\((.*)\)", x).group(1)) for x in df_order_percent_sum.columns)).withColumn("cas_product",lit("order_percent_sum")).cache()
    df_order_all = df_order.unionAll(df_order_percent).unionAll(df_order_percent_sum).cache()

    df_revenue = df_0.groupBy("cas_product").pivot("company").sum("price").na.fill(0).cache()
    df_revenue = df_revenue.withColumn('total', reduce(column_add, (df_revenue[c] for c in df_revenue.columns[1:]))).cache()
    df_revenue_percent = df_revenue.select(*((df_revenue[col] / df_revenue['total']).alias(col) for col in df_revenue.columns[1:])).withColumn("cas_product",lit("df_revenue_percent"))
    df_revenue_percent_sum = df_revenue_percent.agg(
        {col: "sum" for col in df_revenue.columns if col not in ['cas_product']}).cache()
    df_revenue_percent_sum = df_revenue_percent_sum.select(
        *(col(x).alias(re.search(".*\((.*)\)", x).group(1)) for x in df_revenue_percent_sum.columns)).withColumn(
        "cas_product", lit("order_percent_sum")).cache()
    df_revenue_all = df_revenue.unionAll(df_revenue_percent).unionAll(df_revenue_percent_sum).cache()


    # save as csv
    df_order.repartition(1).write.csv(os.path.join(output_path, 'df_order'), header=True)
    df_order_percent.repartition(1).write.csv(os.path.join(output_path, 'df_order_percent'), header=True)
    df_order_percent_sum.repartition(1).write.csv(os.path.join(output_path, 'df_order_percent_sum'), header=True)

    df_revenue.repartition(1).write.csv(os.path.join(output_path, 'df_revenue'), header=True)
    df_revenue_percent.repartition(1).write.csv(os.path.join(output_path, 'df_revenue_percent'), header=True)
    df_revenue_percent_sum.repartition(1).write.csv(os.path.join(output_path, 'df_revenue_percent_sum'), header=True)
    # df_3.saveAsTextFile(output_path)
    spark.stop()




# def rename_cols(agg_df, ignore_first_n=1):
#     delimiters = "(", ")"
#     split_pattern = '|'.join(map(re.escape, delimiters))
#     splitter = partial(re.split, split_pattern)
#     split_agg = lambda x: '_'.join(splitter(x))[0:-ignore_first_n]
#     renamed = map(split_agg, agg_df.columns[ignore_first_n:])
#     renamed = zip(agg_df.columns[ignore_first_n:], renamed)
#     for old, new in renamed:
#         agg_df = agg_df.withColumnRenamed(old, new)
#     return agg_df

# def rename_cols(agg_df, ignore_first_n=1):
#     renamed = map(lambda x: re.search(".*\((.*)\)", x).group(1), agg_df.columns[ignore_first_n:])
#     renamed = zip(agg_df.columns[ignore_first_n:], renamed)
#     for old, new in renamed:
#         agg_df = agg_df.withColumnRenamed(old, new)
#     return agg_df
