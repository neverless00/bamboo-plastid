#!/usr/bin/python

import sys

current_word = None
current_count = 0
word = None

for line in sys.stdin:
    line = line.strip()
    word, count = line.split('\t', 1)

    if current_word == word:
        current_count += int(count)
    else:
        if current_word:
            sys.stdout.write('%s\t%s' % (str(current_word), str(current_count)))
            print('%s\t%s' % (str(current_word), str(current_count)))
        current_count = count
        current_word = word
