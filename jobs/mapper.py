#!/usr/bin/python
import sys

sys.stdout.write("started mapper\n")


def main(argv):
    for line in sys.stdin:
        order_id, company, lnid, price = line.split("|")
        # print("LongValueSum:%s\t1" % company + "-" + lnid)
        key = str(company) + "-" + str(lnid)
        print("%s\t1" % company)


if __name__ == '__main__':
    main(sys.argv)
