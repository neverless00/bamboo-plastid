#!/bin/bash

function jsonval {
    temp=`echo $json | sed 's/\\\\\//\//g' | sed 's/[{}]//g' | awk -v k="text" '{n=split($0,a,","); for (i=1; i<=n; i++) print a[i]}' | sed 's/\"\:\"/\|/g' | sed 's/[\,]/ /g' | sed 's/\"//g' | grep -w $prop | cut -d ":" -f 2`
    echo ${temp##*|}
}

json=`cat /mnt/var/lib/info/instance.json`
prop='isMaster'
ismaster=`jsonval`

if [[ "$ismaster" -eq "true" ]]
then
	#
	# do actions only on master
	#
	cd ~
	mkdir install
	cd install
	sudo yum -y install cmake swig pip
	sleep 5
	sudo pip install pandas

else
	#
	# do actions only on compute hosts
	#
	cd ~
	mkdir install
	cd install
	sudo yum -y install cmake swig pip
	sleep 5
	sudo pip install pandas
fi
